<?php

$servername = "127.0.0.1";
$username   = "it-colors";
$password   = "2147483647";
$dbname     = "highexpress";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "CREATE TABLE `hex_prod_values` (    `id` int(11) NOT NULL,  `catg_id` varchar(60) NOT NULL DEFAULT '',
  `catg_name` varchar(60) NOT NULL DEFAULT '',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `sort_order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
$result = mysqli_query($conn, $sql);
var_dump($result);

$sql = "INSERT INTO `hex_prod_values` (`id`, `catg_id`, `catg_name`, `status`, `sort_order`) VALUES
(1, 'children', 'Children', 'active', 1),
(2, 'senior_people', 'Senior people', 'active', 2),
(3, 'homeless', 'Homeless', 'active', 3),
(4, 'poor', 'Poor people', 'active', 4),
(5, 'rehabiltation', 'Rehabiltation', 'active', 5),
(6, 'creative_activity', 'Creative activity', 'active', 6),
(7, 'сonstruction_works', 'Construction works', 'active', 7),
(8, 'ministry_projects', 'Ministry projects', 'active', 8),
(9, 'health', 'Health', 'active', 9);";
$result = mysqli_query($conn, $sql);

var_dump($result);

$sql = "ALTER TABLE `hex_prod_values` ADD PRIMARY KEY (`id`);";
$result = mysqli_query($conn, $sql);

var_dump($result);

$sql = "ALTER TABLE `hex_prod_values` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;";
$result = mysqli_query($conn, $sql);

var_dump($result);

$sql = "ALTER TABLE `hex_products` ADD `prod_values` VARCHAR(255) NULL AFTER `prod_values`";
$result = mysqli_query($conn, $sql);

var_dump($result);


$sql = "INSERT INTO `hex_langs` (`lang_key`, `english`, `french`, `russian`, `ukraine`) VALUES
('children', 'Children', 'Enfants', 'Дети', 'Діти'),
('senior_people', 'Senior people', 'Personnes âgées', 'Старики', 'Люди похилого віку'),
('homeless', 'Homeless', 'Sans abri', 'Бездомные', 'Безхатні'),
('poor_people', 'Poor people', 'Les pauvres', 'Неимущие', 'Бідні'),
('rehabiltation', 'Rehabiltation', 'Réhabilitation', 'Реабилитация', 'Реабілітація'),
('creative_activity', 'Creative activity', 'Activité créative', 'Творчество', 'Творчість'),
('construction_works', 'Construction works', 'Travaux de construction', 'Строительство', 'Будівництво'),
('ministry_projects', 'Ministry projects', 'Projets du ministère', 'Проекты служения', 'Проекти служіння'),
('health', 'Health', 'Santé', 'Здоровье', 'Здоров'я'),
('values', 'Values', 'Valeurs', 'Ценности', 'Цінності'),
('the_product_values_are_required_please_check_values', 'The product values are required. Please check values', 'Les valeurs des produits sont obligatoires. Veuillez cocher les valeurs', 'Ценности являются обязательным параметром. Отметьте, пожалуйста, ценности товара.', 'Цінності є обов'язковим параметром. Будьласка, виберіть цінності'),
('not_more_than', 'Not more than', 'Pas plus de', 'Не больше', 'Не більше')";
$result = mysqli_query($conn, $sql);

var_dump($result);



$conn->close();