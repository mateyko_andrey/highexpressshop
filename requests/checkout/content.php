<?php
# @*************************************************************************@
# @ @author: Mansur Altamirov (Mansur_TL)									@
# @ @author_url 1: https://www.instagram.com/mansur_tl                      @
# @ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
# @ @author_email: highexpresstore@gmail.com                                @
# @*************************************************************************@
# @ HighExpress - The Ultimate Modern Marketplace Platform                  @
# @ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
# @*************************************************************************@

if (empty($hs['is_logged'])) {
	hs_redirect('auth');
} 

$hs['page_title']    = hs_translate('Checkout - {%name%}',array('name' => $hs['config']['name']));
$hs['page_desc']     = $hs['config']['description'];
$hs['page_kw']       = $hs['config']['keywords'];
$hs['pn']            = 'checkout';
$hs['header_st']     = true;
$my_id               = $me['id'];
$total_items         = hs_basket_items_total($me['id']);
$hs['total_items']   = $total_items;
$hs['basket']        = hs_get_basket_products();
$hs['header_st']     = ((not_empty($hs['basket'])) ? true : false);
$hs['prepaym_prods'] = 0;
$hs['codpaym_prods'] = 0;
$active_items        = array_map(function($item) {
	return $item['id'];
}, $hs['basket']);
$hs['total '] = 0.0;
$hs['total2'] = 0.0;
$hs['sellActiveFlag'] = 0; 
$nn  = 0;
$nn1 = 0;
$nn2 = 0;
$sellerSelected = [];
$selId = 0;
 $deliveryFlag = 0; 
 $deliveryMethod = "";
foreach ($hs['basket'] as $hs['prod_data']){    
    if($selId != $hs['prod_data']['user_id']){
        if($nn2&&($nn2 == $nn1)){
            $sellerSelected[] = $selId;            
        } else {
            $nn2 = $nn1;
        }
        $selId = $hs['prod_data']['user_id'];
    }   
    if($hs['prod_data']['status'] == 'active'){
         $hs['total']+=  $hs['prod_data']['sale_price'];
         $hs['total2']+= $hs['prod_data']['reg_price']?$hs['prod_data']['reg_price']:$hs['prod_data']['sale_price'];
         if($sellActive != $hs['prod_data']['user_id']){ 
            if($sellActive){
                $hs['sellActiveFlag'] = 1;
            }
            $sellActive  = $hs['prod_data']['user_id'];
         }  
         $nn++;
         $nn2++;
         
        if(($deliveryMethod    != $hs['prod_data']['shipping_cost'])){
            if($deliveryMethod){
                $deliveryFlag = 1;
            }
            $deliveryMethod = $hs['prod_data']['shipping_cost'];                                    
        }
         
    }    
    $nn1++;
}
if($deliveryFlag == 1){
    $hs['delivery_mesage'] = hs_translate('Please consult the seller');                                    
} else {
    if($deliveryMethod == 'free'){                                         
        $hs['delivery_mesage'] = hs_translate('Free Shipping');                                    
    } else if($deliveryMethod == 'paid'){
        $hs['delivery_mesage'] = hs_translate('According to the tariffication of the courier service');
    }  else {
        $hs['delivery_mesage'] = 'nothing selected';
    }                                   
}
if($nn2&&($nn2 == $nn1)){
     $sellerSelected[] = $selId;            
}
$hs['discount']        = ($hs['total2'])?floor(($hs['total2']-$hs['total'])*100/$hs['total2']):0;
$hs['all_selected']    = (count($hs['basket']) == $nn)?'1':'0';
$hs['seller_selected'] = $sellerSelected;

if (not_empty($hs['basket'])) {
	foreach ($hs['basket'] as $row) {
		if ($row['payment_method'] == 'pre_payments') {
			$hs['prepaym_prods'] += 1;
		}

		else if($row['payment_method'] == 'cod_payments') {
			$hs['codpaym_prods'] += 1;
		}
	}
}

foreach ($hs['countries'] as $id => $country_name){
    if($hs['me']['country_id'] == $id){
        $hs['me_country'] = hs_translate($country_name);
    }
}

/*if(ip_info()){
   $hs['me_country'] = ip_info();
}*/
$unset_payment_data =  hs_session_unset('payment_data');
$hs['addresses']    =  hs_get_user_addresses($my_id);
$hs['deliv_addr']   =  implode(",",array($me['address']['country'],$me['address']['city']));
$hs['site_content'] =  hs_loadpage('checkout/content',array(
	'total_items'   => $total_items
)); 

function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}