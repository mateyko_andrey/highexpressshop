<?php 
# @*************************************************************************@
# @ @author Mansur Altamirov (Mansur_TL)									@
# @ @author_url 1: https://www.instagram.com/mansur_tl                      @
# @ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
# @ @author_email: highexpresstore@gmail.com                                @
# @*************************************************************************@
# @ HighExpress - The Ultimate Modern Marketplace Platform                  @
# @ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
# @*************************************************************************@

session_start();
error_reporting(E_ALL);
require_once("../core/libs/DB/vendor/autoload.php");
require_once("../core/utils/utils.php");
require_once("languages.php");

$hex_tables = array(
	'hex_users',
	'hex_sessions',
	'hex_langs',
	'hex_config',
	'hex_faqs',
	'hex_settings',
	'hex_products',
	'hex_prod_variations',
	'hex_product_media',
	'hex_basket',
	'hex_orders',
	'hex_market_revenue',
	'hex_checkout_transactions',
	'hex_wishlist',
	'hex_wishlist_items',
	'hex_deliv_addresses',
	'hex_ord_hist_timeline',
	'hex_announcements',
	'hex_temp_data',
	'hex_notifications',
	'hex_chat_conversations',
	'hex_chat_messages',
	'hex_prod_ratings',
	'hex_prod_rating_media',
	'hex_prod_reports',
	'hex_payout_requests',
	'hex_admins',
	'hex_data_sessions',
	'hex_backups',
	'hex_acc_del_requests',
	'hex_admin_sessions',
	'hex_static_pages',
	'hex_store_customers',
	'hex_order_cancellations',
	'hex_temp_media',
	'hex_prod_categories',
	'hex_languages',
	'hex_currencies',
	'hex_blocked_users',
);