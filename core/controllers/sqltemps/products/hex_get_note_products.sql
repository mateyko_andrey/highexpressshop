/*@*************************************************************************@
//@ @author Mansur Altamirov (Mansur_TL)									@
//@ @author_url 1: https://www.instagram.com/mansur_tl                      @
//@ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
//@ @author_email: highexpresstore@gmail.com                                @
//@*************************************************************************@
//@ HighExpress - The Ultimate Modern Marketplace Platform                  @
//@ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
//@*************************************************************************@*/
{% if condition == 'status' %}
  SELECT COUNT(prod.`id`) as counter FROM `{%t_products%}` prod WHERE prod.`status` = 'deleted'
{% endif %}
{% if condition == 'approved' %}
  SELECT COUNT(prod.`id`) as counter FROM `{%t_products%}` prod WHERE prod.`approved` = 'N' AND  prod.`editing_stage` = 'saved'  AND  prod.`status` = 'active'  AND  prod.`is_gcs` = '0'
{% endif %}
{% if condition == 'allproducts' %}
  SELECT COUNT(prod.`id`) as counter FROM `{%t_products%}` prod WHERE prod.`activity_status` = 'active' AND  prod.`editing_stage` = 'saved'  AND  prod.`status` IN ('active','inactive','blocked')  AND  prod.`is_gcs` = '0'
{% endif %}
{% if condition == 'draft' %}
  SELECT COUNT(prod.`id`) as counter FROM `{%t_products%}` prod WHERE prod.`editing_stage` = 'unsaved'  AND  prod.`status` = 'active'  AND  prod.`user_id` = '{% recepient %}'  AND  prod.`activity_status` = 'active'  AND  prod.`is_gcs` = '0'
{% endif %}
{% if condition == 'my_products' %}
  SELECT COUNT(prod.`id`) as counter FROM `{%t_products%}` prod WHERE prod.`editing_stage` = 'saved'  AND  prod.`user_id` = '{% recepient %}'  AND  prod.`activity_status` = 'active'  AND  prod.`is_gcs` = '0'
{% endif %}
{% if condition == 'valid' %}
  SELECT COUNT(prod.`id`) as counter FROM `{%t_products%}` prod WHERE prod.`approved` = 'Y'  AND  prod.`status` = 'active'  AND  prod.`user_id` = '{% recepient %}'  AND  prod.`activity_status` = 'active' AND prod.`editing_stage` = 'saved'  AND  prod.`is_gcs` = '0'
{% endif %}
{% if condition == 'alldraft' %}
  SELECT COUNT(prod.`id`) as counter FROM `{%t_products%}` prod WHERE prod.`editing_stage` = 'unsaved'  AND  prod.`status` = 'active'  AND  prod.`activity_status` = 'active'  AND  prod.`is_gcs` = '0'
{% endif %}