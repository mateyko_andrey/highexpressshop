/*@*************************************************************************@
//@ @author Mansur Altamirov (Mansur_TL)				    @
//@ @author_url 1: https://www.instagram.com/mansur_tl                      @
//@ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
//@ @author_email: highexpresstore@gmail.com                                @
//@*************************************************************************@
//@ HighExpress - The Ultimate Modern Marketplace Platform                  @
//@ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
//@*************************************************************************@*/

SELECT 	COUNT(notif.`id`) as counter FROM `{%t_notifs%}` notif WHERE notif.`subject` = '{%subject%}' AND notif.`status` = '0' AND notif.`recipient_id` = '{%recepient%}'