<?php 
# @*************************************************************************@
# @ @author Mansur Altamirov (Mansur_TL)                                    @
# @ @author_url 1: https://www.instagram.com/mansur_tl                      @
# @ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
# @ @author_email: highexpresstore@gmail.com                                @
# @*************************************************************************@
# @ HighExpress - The Ultimate Modern Marketplace Platform                  @
# @ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
# @*************************************************************************@

function hs_notify($data = array(),$rm_duplicate = false) {
	global $db,$hs,$me;
	if (empty($hs['is_logged'])) {
		return false;
	}
        
	if ($rm_duplicate) {
		$duplicate         = array(
			'notifier_id'  => $data['notifier_id'],
			'recipient_id' => $data['recipient_id'],
			'subject'      => $data['subject'],
		);

		foreach ($duplicate as $key => $val) {
			$db->where($key,$val);
		}

		$db->delete(T_NOTIFS);
	}
	$insert_id = $db->insert(T_NOTIFS,$data);
	return $insert_id;
}

function hs_get_unseen_notifications() {
	global $db,$hs,$me;
	if (empty($hs['is_logged'])) {
		return 0;
	}

	$user_id = $me['id'];
	$db      = $db->where('recipient_id',$user_id);
	$db      = $db->where('status','0');
	$total   = $db->getValue(T_NOTIFS,"COUNT(*)");
	return ((is_numeric($total)) ? $total : 0);
}

function hs_get_notifications($args = array()) {
	global $db,$hs;
	$args              =  (is_array($args)) ? $args : array();
	$options           =  array(
        "offset"       => null,
        "limit"        => 10,
        "recipient_id" => null,
    );

    $args              =  array_merge($options, $args);
    $offset            =  $args['offset'];
    $limit             =  $args['limit'];
    $recipient_id      =  $args['recipient_id'];
    $data              =  array();
    $sql               =  hs_sqltepmlate('notifs/hex_get_rt_notifications',array(
        'offset'       => $offset,
        'limit'        => $limit,
        'recipient_id' => $recipient_id,
        't_users'      => T_USERS,
        't_notifs'     => T_NOTIFS,
    ));

    $notifs = $db->rawQuery($sql);

    if (hs_queryset($notifs) == true) {
    	foreach ($notifs as $notif_data) {
    		$notif_data->notifier_avatar = hs_get_media($notif_data->notifier_avatar);
    		$notif_data->time            = date('d M, y',$notif_data->time);
    		$notif_data->message         = hs_translate($notif_data->message);
    	}

        $data = hs_o2array($notifs); 
    }

    return $data;
}
function hs_get_products_notifications() {
    global $db,$hs,$me;
    $sql              =  hs_sqltepmlate('products/hex_get_note_products',array(
        'condition'   => 'approved',
        't_products'  =>  T_PRODUCTS,
    ));    
    $new = $db->rawQuery($sql);  
    $sql              =  hs_sqltepmlate('products/hex_get_note_products',array(
        'condition'   => 'allproducts',
        't_products'  =>  T_PRODUCTS,
    ));
    $allproducts = $db->rawQuery($sql);  
    $sql              =  hs_sqltepmlate('products/hex_get_note_products',array(
        'condition'   => 'status',
        't_products'  =>  T_PRODUCTS,
    ));     
    $deleted = $db->rawQuery($sql);
        $sql              =  hs_sqltepmlate('products/hex_get_note_products',array(
        'condition'   => 'my_products',
        't_products'  =>  T_PRODUCTS,
        'recepient'   => $me['id'],    
    ));     
    $myProduts = $db->rawQuery($sql);
    $sql              =  hs_sqltepmlate('products/hex_get_note_products',array(
        'condition'   => 'draft',
        'recepient'   => $me['id'],
        't_products'  =>  T_PRODUCTS,
    ));
    $drafts = $db->rawQuery($sql);
    $sql              =  hs_sqltepmlate('products/hex_get_note_products',array(
        'condition'   => 'alldraft',
        'recepient'   => $me['id'],
        't_products'  =>  T_PRODUCTS,
    ));
    $alldrafts = $db->rawQuery($sql);     
     $sql              =  hs_sqltepmlate('products/hex_get_note_products',array(
        'condition'   => 'valid',
        'recepient'   => $me['id'],
        't_products'  =>  T_PRODUCTS,
    ));
    $approved = $db->rawQuery($sql);
    
    
    
    $allMerchants         = hs_ap_info_get_total_merchants();
    $allUsers             = hs_ap_info_get_total_users();
    $allModerators        = hs_ap_info_get_total_moders();
    $reclamaCount         = count(hs_get_gcs_products(['limit'=> 177]));
    $reportsCount         = count(hs_ap_info_get_customer_reports());
    $accountToDel         = hs_ap_info_get_account_removals_tatal();
    $accountVerifications = hs_ap_info_get_account_verification_tatal();
    $returns              = hs_get_customer_refunds_total();
    $custOrders           = hs_get_customer_orders_total();
    $myOrders             = hs_get_my_orders_total();
    //var_dump($drafts[0]->counter);
    $total   = intval($deleted[0]->counter) + intval($new[0]->counter + $accountToDel + $accountVerifications + $returns);
    
    
    
    return [
        'new'                  => $new[0]->counter,
        'deleted'              => $deleted[0]->counter, 
        'total'                => $total,
        'drafts'               => $drafts[0]->counter, 
        'approved'             => $approved[0]->counter,
        'alldrafts'            => $alldrafts[0]->counter,
        'my_products'          => $myProduts[0]->counter, 
        'allproducts'          => $allproducts[0]->counter,
        'allmerchants'         => $allMerchants,
        'allusers'             => $allUsers,
        'allmoderators'        => $allModerators,
        'reclamacount'         => $reclamaCount,
        'reportscount'         => $reportsCount,
        'accountToDel'         => $accountToDel,
        'accountVerifications' => $accountVerifications,
        'returns'              => $returns,
        'custorders'           => $custOrders,
        'myorders'             => $myOrders
            ];
    
    
    
}

function hs_set_seen_products_notifications($subject) {
    global $db,$hs,$me;
   $sql               =  hs_sqltepmlate('notifs/hex_set_seen_protucts_notifications',array(
        'subject'     => $subject,
        'recepient'   => $me['id'],
        't_notifs'    => T_NOTIFS,
    )); 
    $seen = $db->rawQuery($sql);
    
    return $seen;
}