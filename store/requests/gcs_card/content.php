<?php
# @*************************************************************************@
# @ @author Mansur Altamirov (Mansur_TL)									@
# @ @author_url 1: https://www.instagram.com/mansur_tl                      @
# @ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
# @ @author_email: highexpresstore@gmail.com                                @
# @*************************************************************************@
# @ HighExpress - The Ultimate Modern Marketplace Platform                  @
# @ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
# @*************************************************************************@
if($hs['me']['admin']){
    $hs['pn']           =  $store_page;
    $hs['page_title']   =  hs_translate('Draft - {%name%}',array('name' => $hs['me']['name']));
    $hs['page_desc']    =  $hs['config']['description'];
    $hs['page_kw']      =  $hs['config']['keywords'];
    $hs['my_prods']     =  hs_get_gcs_products(array(
            'limit'         => 17, 
    ));

    $hs['my_draft_tot'] = hs_get_draft_products_total($me['id']);
    $hs['site_content'] = hs_loadpage("gcs_card/content");
} else{
    hs_redirect('home');
}