<?php
# @*************************************************************************@
# @ @author Mansur Altamirov (Mansur_TL)                                    @
# @ @author_url 1: https://www.instagram.com/mansur_tl                      @
# @ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
# @ @author_email: highexpresstore@gmail.com                                @
# @*************************************************************************@
# @ HighExpress - The Ultimate Modern Marketplace Platform                  @
# @ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
# @*************************************************************************@
if(empty($hs['is_seller'])) {
	hs_redirect('/');
}
$hs['pn']           =  $store_page;
$hs['page_title']   =  hs_translate('Customer orders');
$hs['page_desc']    =  $hs['config']['description'];
$hs['page_kw']      =  $hs['config']['keywords'];
$orders_total       = $db->getValue(T_ORDERS,'COUNT(*)');
$hs['orders_total'] = intval($orders_total);

$hs['orders_list']  =  hs_get_customer_orders(array(
	'seller_id'     => $me['id'],
	'limit'         => 10,
        'offset'        => 0
));

$hs['site_content'] = hs_loadpage("customer_orders/content");