<?php
# @*************************************************************************@
# @ @author Mansur Altamirov (Mansur_TL)									@
# @ @author_url 1: https://www.instagram.com/mansur_tl                      @
# @ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
# @ @author_email: highexpresstore@gmail.com                                @
# @*************************************************************************@
# @ HighExpress - The Ultimate Modern Marketplace Platform                  @
# @ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
# @*************************************************************************@

if (empty($_GET['id']) || is_numeric($_GET['id']) != true) {
	hs_redirect('404');
} 

elseif ((hs_is_prodowner($me['id'],$_GET['id']) !== true)&&($hs['is_admin']!=1)) {
	hs_redirect('404');
}

$upsert_type	 = hs_session("upsert_type","edit");
$prod_id         = intval($_GET['id']);
$hs['prod_data'] = hs_product_form_data($prod_id);
if (not_empty($hs['prod_data'])) {
	$hs['pn']               =  $store_page;
	$hs['page_title']       =  hs_translate('Edit - {%name%}',array('name' => $hs['prod_data']['name']));
	$hs['page_desc']        =  $hs['config']['description'];
	$hs['page_kw']          =  $hs['config']['keywords'];
	$hs['prod_id']          =  $prod_id;

	$hs['site_content']     =  hs_loadpage("upsert_gcs_card/update",array(
		"prod_id"           => $hs['prod_id'],
		"prod_name"         => $hs['prod_data']['name'],
		"prod_desc"         => hs_br2nl($hs['prod_data']['description']),
		"prod_reg_price"    => $hs['prod_data']['reg_price'],
		"prod_sale_price"   => $hs['prod_data']['sale_price'],
		"prod_quantity"     => $hs['prod_data']['quantity'],
		"prod_origin"       => $hs['prod_data']['origin'],
		"prod_brand"        => $hs['prod_data']['brand'],
		"prod_model_number" => $hs['prod_data']['model_number'],
		"prod_weight"       => $hs['prod_data']['weight'],
		"prod_length"       => $hs['prod_data']['length'],
		"prod_width"        => $hs['prod_data']['width'],
		"prod_height"       => $hs['prod_data']['height'],
		"prod_poster"       => $hs['prod_data']['poster'],
		"sku"               => $hs['prod_data']['sku'],
                "card_link"         => $hs['prod_data']['card_link'],
                "selValues"         => explode(',', $hs['prod_data']['prod_values'])
	));

	hs_session('edit_product_data',array('prod_id' => $prod_id));
	hs_setprod_val($prod_id,array('editing_stage' => 'unsaved'));
        if($hs['is_admin']){            
            hs_notify(array(
                 'notifier_id'  => $me['id'],
                 'recipient_id' => $hs['prod_data']['user_id'],
                 'subject'      => 'moved_to_draft',
                 'message'      => 'This product was moved to drafts by admin',
                 'status'       => '0',
                 'time'         => time(),
                 'url'          => hs_link(sprintf('merchant_panel/products_draft'))
            ),false); 
        }
} 

else{
	echo "Something went wrong. Please try again later!";
	exit;
}